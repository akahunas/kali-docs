---
title: NetHunter Chroot Manager
description:
icon:
date: 2019-11-29
type: post
weight: 195
author: ["re4son",]
tags: ["",]
keywords: ["",]
og_description:
---

The NetHunter chroot manager allows you to download and install a Kali Linux chroot (if one does not already exist), backup and restore a chroot, as well as remove an existing chroot. In addition, one can install various Kali Linux metapackages as required.

![](./nethunter-chroot-01.png)

In general, the "kali-nethunter" metapackage contains everything needed to run NetHunter, so be sure to only add extra metapackages if they're really needed, especially if disk space is at a premium. To get a general idea of the disk space required for each of the metapackages, refer to the [Kali Linux Metapackages blog post](https://www.kali.org/news/kali-linux-metapackages/).

.![](./nethunter-chroot-02.png)
